package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String getType() {
        return "Defend with shield";
    }

    @Override
    public String defend() {
        return "oof... tangan pegel...";
    }

}