package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String getType() {
        return "Attack with magic";
    }

    @Override
    public String attack() {
        return "Sim Salabim!";
    }
}