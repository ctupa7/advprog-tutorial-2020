package id.ac.ui.cs.advprog.tutorial1.strategy.core;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class KnightAdventurer extends Adventurer {
    @Autowired
    private StrategyRepository strategyRepository;

    public KnightAdventurer() {
        super.setAttackBehavior(new AttackWithSword());
        super.setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return "Knight Adventurer";
    }

}
