package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String getType() {
        return "Attack with sword";
    }

    @Override
    public String attack() {
        return "*Wung* *Wung*";
    }
}