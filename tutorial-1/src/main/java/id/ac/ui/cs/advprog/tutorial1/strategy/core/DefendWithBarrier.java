package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String getType() {
        return "Defend with barrier";
    }

    @Override
    public String defend() {
        return "oof... magic berkurang...";
    }

}
