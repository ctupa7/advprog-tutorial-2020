package id.ac.ui.cs.advprog.tutorial1.strategy.core;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class MysticAdventurer extends Adventurer {
    @Autowired
    private StrategyRepository strategyRepository;

    public MysticAdventurer() {
        super.setAttackBehavior(new AttackWithMagic());
        super.setDefenseBehavior(new DefendWithShield());;
    }

    @Override
    public String getAlias() {
        return "Mystic Adventurer";
    }

}
